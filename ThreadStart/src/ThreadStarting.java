
public class ThreadStarting {

	/*
	 * shows different ways to create a runnable and pass it to a thread
	 */
	public static void main(String[] args) {

		Thread[] t = new Thread[6];

		t[0] = new Thread(new Runnable() {
			@Override
			public void run() {
				doSomething();
			}
		});

		t[1] = new Thread(() -> {
			doSomething();
		});

		t[2] = new Thread(() -> doSomething());

		t[3] = new Thread(ThreadStarting::doSomething);

		Printer printer = new Printer();

		String msg = "Hello, World";
		t[4] = new Thread(() -> printer.doSomething(msg));

		t[5] = new Thread(printer::doSomethingElse);

		for (Thread a : t) {
			if (a != null)
				a.start();
		}

	}

	static void doSomething() {
		System.out.printf("This does something. (%s)\n", Thread.currentThread().getName());
	}

}

class Printer {
	public void doSomething(String message) {
		System.out.printf("%s says %s\n", Thread.currentThread().getName(), message);
	}

	public void doSomethingElse() {
		this.doSomething("Yet another method!");
	}
}
